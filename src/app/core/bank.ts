export interface Bank {
    bank?: string;
    number?: string;
    type?: string;
    name?: string;
    ci?: string;
    email?: string;
    phoneNumber?: string;
    data?: string;
}
