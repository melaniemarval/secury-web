export interface Note {
    name?: string;
    content?: string;
    color?: string;
}
