export interface AccountInterface {
    id?: string;
    name?: string;
    email?: string;
    username?: string;
    password?: string;
    data?: string;
    iconUrl?: string;
}
