export interface Card {
    bank?: string;
    cardNumber?: string;
    expirationDate?: string;
    cvc?: string;
    type?: string;
    name?: string;
    ci?: string;
    phoneNumber?: string;
    address?: string;
    data?: string;
    color?: any;
}
