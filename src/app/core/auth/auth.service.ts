import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';

@Injectable({
    providedIn: 'root',
})
export class AuthService {

    constructor(private afAuth: AngularFireAuth) {
    }

    async loginWithGoogle(): Promise<any> {
        return await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    }

    async logout(): Promise<any> {
        return await this.afAuth.signOut();
    }
}
