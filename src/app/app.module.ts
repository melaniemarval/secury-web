import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './global/app.material.module';
import { CardComponentsModule } from './shared/card-components/card-components.module';
import { IConfig, NgxMaskModule } from 'ngx-mask';
// Dialogs
import { DialogConfirmExitModule } from './shared/toolbar/dialog-confirm-exit/dialog-confirm-exit.module';
import { HttpClientModule } from '@angular/common/http';
import { AppFirebaseModule } from './global/app-firebase.module';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppMaterialModule,
        AppFirebaseModule,
        CardComponentsModule,
        DialogConfirmExitModule,
        NgxMaskModule.forRoot(options),
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {
}
