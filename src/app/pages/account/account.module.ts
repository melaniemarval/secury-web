import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account.component';
import { FormsModule } from '@angular/forms';
import { AppMaterialModule } from '../../global/app.material.module';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { CardComponentsModule } from '../../shared/card-components/card-components.module';
import { IconSelectorComponent } from './icon-selector/icon-selector.component';



@NgModule({
    declarations: [
        AccountComponent,
        IconSelectorComponent
    ],
    exports: [
        AccountComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        CardComponentsModule,
        AppMaterialModule,
        PipesModule,
    ],
})
export class AccountModule { }
