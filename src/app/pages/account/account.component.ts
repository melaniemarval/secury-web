import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
// Material
import { Clipboard } from '@angular/cdk/clipboard';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
// Services
import { AccountInterface } from '../../core/accountInterface';
import { ContentService } from '../../global/providers/content.service';
import { ActionCard } from '../../global/consts/action-card.enum';

import { ConfirmDeleteComponent } from '../../shared/card-components/confirm-delete/confirm-delete.component';
import { IconSelectorComponent } from './icon-selector/icon-selector.component';
import { AccountIcons } from '../../../assets/icon/accounts/icons';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnInit {

    showPass = false;
    account: AccountInterface;
    action: ActionCard = ActionCard.ADD;
    actionCardType = ActionCard;

    constructor(private contentService: ContentService,
                private dialog: MatDialog,
                private snackBar: MatSnackBar,
                private clipboard: Clipboard) {
    }

    ngOnInit(): void {
        this.contentService.selectCard.subscribe(card => {
            this.account = {...card};
        });
        this.contentService.actionCard.subscribe((action: ActionCard) => {
            this.action = action;
        });
    }

    onSubmit(form: NgForm): void {
        this.contentService.actionCard.emit(ActionCard.VIEW);
        if (this.action === ActionCard.ADD) {
        }
    }

    changeActionToEdit(): void {
        this.contentService.actionCard.emit(ActionCard.EDIT);
    }

    confirmDelete(): void {
        const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
            data: this.account,
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.deleteAccount();
            }
        });
    }

    private deleteAccount(): void {
        this.contentService.selectCard.emit(undefined);
    }

    copyToClipboard(text: string): void {
        this.clipboard.copy(text);
        this.snackBar.open('Texto copiado en el portapapeles');
    }

    selectIcon(): void {
        console.log('-> open dialog with images');
        const dialogRef = this.dialog.open(IconSelectorComponent, {
            width: '300px',
            autoFocus: false
        });

        dialogRef.afterClosed().subscribe((icon: AccountIcons) => {
            console.log('The dialog was closed', icon);
            if (icon) {
                this.account.iconUrl = icon.src;
                this.account.name = icon.name;
            }
        });
    }
}
