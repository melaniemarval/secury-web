import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MaterialIconsService } from '../../../global/providers/material-icons.service';
import { ACCOUNT_ICONS, AccountIcons } from '../../../../assets/icon/accounts/icons';

@Component({
    selector: 'app-icon-selector',
    templateUrl: './icon-selector.component.html',
    styleUrls: ['./icon-selector.component.scss'],
})
export class IconSelectorComponent implements OnInit {

    icons: AccountIcons[] = [];
    search: string;

    constructor(private dialogRef: MatDialogRef<IconSelectorComponent>,
                @Inject(MAT_DIALOG_DATA) private data: any,
                private materialIcons: MaterialIconsService) {
        this.icons = ACCOUNT_ICONS;
    }

    ngOnInit(): void {
    }

    selectIcon(icon: string): void {
        this.dialogRef.close(icon);
    }
}
