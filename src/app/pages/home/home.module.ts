import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { MenuModule } from '../../shared/menu/menu.module';


@NgModule({
    declarations: [HomeComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([{path: '', component: HomeComponent}]),
        MenuModule,
    ],
})
export class HomeModule {
}
