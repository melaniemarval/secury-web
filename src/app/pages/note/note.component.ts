import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
// Material
import { Clipboard } from '@angular/cdk/clipboard';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
// Services
import { ContentService } from '../../global/providers/content.service';
import { ActionCard } from '../../global/consts/action-card.enum';

import { ConfirmDeleteComponent } from '../../shared/card-components/confirm-delete/confirm-delete.component';
import { Note } from '../../core/note';

@Component({
    selector: 'app-note',
    templateUrl: './note.component.html',
    styleUrls: ['./note.component.scss'],
})
export class NoteComponent implements OnInit {

    @ViewChild('textareaContent') textareaRef: ElementRef;
    note: Note;
    action: ActionCard = ActionCard.ADD;
    actionCardType = ActionCard;

    constructor(private contentService: ContentService,
                private dialog: MatDialog,
                private snackBar: MatSnackBar,
                private clipboard: Clipboard) {
    }

    ngOnInit(): void {
        this.contentService.selectCard.subscribe(note => {
            console.log('-> card', note);
            this.note = {...note};
        });
        this.contentService.actionCard.subscribe((action: ActionCard) => {
            this.action = action;
        });
    }

    onSubmit(form: NgForm): void {
        this.contentService.actionCard.emit(ActionCard.VIEW);
        if (this.action === ActionCard.ADD) {
        }
    }

    changeActionToEdit(): void {
        this.contentService.actionCard.emit(ActionCard.EDIT);
        setTimeout(() => { // this will make the execution after the above action has changed
            this.textareaRef.nativeElement.focus();
        }, 0);
    }

    confirmDelete(): void {
        const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
            data: this.note,
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.deleteAccount();
            }
        });
    }

    private deleteAccount(): void {
        this.contentService.selectCard.emit(undefined);
    }

    changeColor(color): void {
        console.log('-> color', color);
        this.note.color = color;
        // const dialogRef = this.dialog.open(ColorSelectorComponent, {
        //     data: [{title: 'Classic Green', color: 'red'}, {title: 'Classic Blue', color: 'green'},
        //         {title: 'Classic Purple', color: 'red'}, {title: 'Gold', color: 'red'},
        //         {title: 'Platinum', color: 'green'}, {title: 'Black', color: 'green'}],
        //     width: '300px',
        //     autoFocus: false,
        // });
        //
        // dialogRef.afterClosed().subscribe(result => {
        //     console.log('The dialog was closed', result);
        //     this.note.color = result.color;
        // });
    }
}
