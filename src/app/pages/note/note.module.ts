import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppMaterialModule } from '../../global/app.material.module';
import { NoteComponent } from './note.component';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { CardComponentsModule } from '../../shared/card-components/card-components.module';


@NgModule({
    declarations: [NoteComponent],
    exports: [
        NoteComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        CardComponentsModule,
        AppMaterialModule,
        PipesModule,
    ],
})
export class NoteModule { }
