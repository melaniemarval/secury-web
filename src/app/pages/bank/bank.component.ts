import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
// Material
import { Clipboard } from '@angular/cdk/clipboard';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
// Services
import { ContentService } from '../../global/providers/content.service';
import { ActionCard } from '../../global/consts/action-card.enum';
import { Bank } from '../../core/bank';

import { ConfirmDeleteComponent } from '../../shared/card-components/confirm-delete/confirm-delete.component';

@Component({
    selector: 'app-bank',
    templateUrl: './bank.component.html',
    styleUrls: ['./bank.component.scss'],
})
export class BankComponent implements OnInit {

    optionsType: string[] = ['Ahorro', 'Corriente'];
    bank: Bank;
    action: ActionCard = ActionCard.ADD;
    actionCardType = ActionCard;

    constructor(private contentService: ContentService,
                private dialog: MatDialog,
                private snackBar: MatSnackBar,
                private clipboard: Clipboard) {
    }

    ngOnInit(): void {
        this.contentService.selectCard.subscribe(card => {
            console.log('-> card', card);
            this.bank = {...card};
        });
        this.contentService.actionCard.subscribe((action: ActionCard) => {
            this.action = action;
        });
    }

    onSubmit(form: NgForm): void {
        this.contentService.actionCard.emit(ActionCard.VIEW);
        if (this.action === ActionCard.ADD) {
        }
    }

    changeActionToEdit(): void {
        this.contentService.actionCard.emit(ActionCard.EDIT);
    }

    confirmDelete(): void {
        const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
            data: this.bank,
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.deleteAccount();
            }
        });
    }

    private deleteAccount(): void {
        this.contentService.selectCard.emit(undefined);
    }

    copyToClipboard(): void {
        let textData = `${this.bank.bank} \n`;
        if (this.bank.number) {
            textData += `${this.bank.number} \n`;
        }
        if (this.bank.type) {
            textData += `${this.bank.type} \n`;
        }
        textData += `${this.bank.name} \n`;
        if (this.bank.ci) {
            textData += `${this.bank.ci} \n`;
        }
        if (this.bank.email) {
            textData += `${this.bank.email} \n`;
        }
        if (this.bank.phoneNumber) {
            textData += `${this.bank.phoneNumber} \n`;
        }
        if (this.bank.data) {
            textData += `Datos adicionales: ${this.bank.data} \n`;
        }

        this.clipboard.copy(textData);
        this.snackBar.open('Texto copiado en el portapapeles');
    }

}
