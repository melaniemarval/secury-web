import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppMaterialModule } from '../../global/app.material.module';
import { CardComponent } from './card.component';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { CardComponentsModule } from '../../shared/card-components/card-components.module';
import { NgxMaskModule } from 'ngx-mask';


@NgModule({
    declarations: [CardComponent],
    exports: [
        CardComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        CardComponentsModule,
        AppMaterialModule,
        PipesModule,
        NgxMaskModule,
    ],
})
export class CardModule { }
