import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
// Material
import { Clipboard } from '@angular/cdk/clipboard';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
// Services
import { Card } from '../../core/card';
import { ContentService } from '../../global/providers/content.service';
import { ActionCard } from '../../global/consts/action-card.enum';

import { ConfirmDeleteComponent } from '../../shared/card-components/confirm-delete/confirm-delete.component';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {

    form: NgForm;
    card: Card;
    action: ActionCard = ActionCard.ADD;
    actionCardType = ActionCard;
    cardOptions: { title: string, color: string }[] = [
        {title: 'Classic Green', color: 'green'},
        {title: 'Classic Blue', color: 'blue'},
        {title: 'Classic Purple', color: 'purple'},
        {title: 'Gold', color: 'gold'},
        {title: 'Platinum', color: 'platinum'},
        {title: 'Black', color: 'black'},
    ];

    constructor(private contentService: ContentService,
                private dialog: MatDialog,
                private snackBar: MatSnackBar,
                private clipboard: Clipboard) {
    }

    ngOnInit(): void {
        this.contentService.selectCard.subscribe(card => {
            console.log('-> card', card);
            this.card = {...card};
            if (!this.card.color) {
                this.card.color = {title: 'Classic Green', color: 'green'};
            }
        });
        this.contentService.actionCard.subscribe((action: ActionCard) => {
            this.action = action;
        });
    }

    onSubmit(form: NgForm): void {
        console.log('-> form', form);
        const cardNew = this.card;
        this.contentService.actionCard.emit(ActionCard.VIEW);
        if (this.action === ActionCard.ADD) {
        }
    }

    changeActionToEdit(): void {
        this.contentService.actionCard.emit(ActionCard.EDIT);
    }

    confirmDelete(): void {
        const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
            data: this.card,
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.deleteAccount();
            }
        });
    }

    private deleteAccount(): void {
        this.contentService.selectCard.emit(undefined);
    }

    copyToClipboard(text: string): void {
        this.clipboard.copy(text);
        this.snackBar.open('Texto copiado en el portapapeles');
    }

}
