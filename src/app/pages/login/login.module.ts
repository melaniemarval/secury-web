import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { AppMaterialModule } from '../../global/app.material.module';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
    declarations: [LoginComponent],
    imports: [
        CommonModule,
        HttpClientModule,
        RouterModule.forChild([{path: '', component: LoginComponent}]),
        AppMaterialModule,
    ],
})
export class LoginModule {
}
