import { Component, OnInit } from '@angular/core';
import { MaterialIconsService } from '../../global/providers/material-icons.service';
import { AuthService } from '../../core/auth/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

    constructor(private materialIconsService: MaterialIconsService,
                private authService: AuthService) {
    }

    ngOnInit(): void {
    }

    redirectToGooglePlay(): void {
        console.log('-> redirectToGooglePlay');

    }

    login(): void {
        this.authService.loginWithGoogle().then(result => {
            console.log('-> res', result);
            // tslint:disable-next-line:no-redundant-jsdoc
            /** @type {firebase.auth.OAuthCredential} */
            const credential = result.credential;

            // This gives you a Google Access Token. You can use it to access the Google API.
            const token = credential.accessToken;
            // The signed-in user info.
            const user = result.user;
        }).catch(error => {
            console.log('-> error', error);
            const errorCode = error.code;
            const errorMessage = error.message;
            // The email of the user's account used.
            const email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            const credential = error.credential;
        });
    }
}
