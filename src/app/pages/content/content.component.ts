import { Component, OnInit } from '@angular/core';
import { ContentService } from '../../global/providers/content.service';
import { Tab } from '../../global/consts/tab.enum';
import { ActionCard } from '../../global/consts/action-card.enum';

@Component({
    selector: 'app-content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.scss'],
})
export class ContentComponent implements OnInit {

    tabType = Tab;
    tabSelected = 0;
    actionCard: ActionCard = ActionCard.VIEW;
    actionCardType = ActionCard;

    constructor(private service: ContentService) {
    }

    fillerContent = Array.from({length: 3}, () =>
        `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
       labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
       laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
       voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
       cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`);

    ngOnInit(): void {
        this.doSubscriptions();
    }

    private doSubscriptions(): void {
        this.service.changeTab.subscribe(index => {
            console.log('-> index', index);
            this.tabSelected = index;
            this.service.actionCard.emit(ActionCard.VIEW);
        });

        this.service.actionCard.subscribe(card => {
            console.log('-> card', card);
            this.actionCard = card;
        });
    }

    addNewItem(): void {
        this.service.actionCard.emit(ActionCard.ADD);
        this.service.selectCard.emit({});
    }
}
