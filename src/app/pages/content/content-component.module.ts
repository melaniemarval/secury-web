import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content.component';
import { AppMaterialModule } from '../../global/app.material.module';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';

// Components
import { AccountModule } from '../account/account.module';
import { CardModule } from '../card/card.module';
import { BankModule } from '../bank/bank.module';
import { NoteModule } from '../note/note.module';



@NgModule({
    declarations: [ContentComponent],
    imports: [
        CommonModule,
        AppMaterialModule,
        AccountModule,
        CardModule,
        BankModule,
        NoteModule,
    ],
    exports: [
        ContentComponent,
    ],
    providers: [
        {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 25000}}
    ]
})
export class ContentComponentModule { }
