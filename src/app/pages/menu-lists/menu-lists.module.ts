import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountsComponent } from './accounts/accounts.component';
import { CardsComponent } from './cards/cards.component';
import { BanksComponent } from './banks/banks.component';
import { NotesComponent } from './notes/notes.component';
import { AppMaterialModule } from '../../global/app.material.module';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { WithoutListComponent } from './without-list/without-list.component';
import { FormsModule } from '@angular/forms';


@NgModule({
    declarations: [
        AccountsComponent,
        CardsComponent,
        BanksComponent,
        NotesComponent,
        SearchbarComponent,
        WithoutListComponent
    ],
    exports: [
        CardsComponent,
        AccountsComponent,
        BanksComponent,
        NotesComponent,
        SearchbarComponent,
        WithoutListComponent
    ],
    imports: [
        CommonModule,
        AppMaterialModule,
        FormsModule,
    ],
})
export class MenuListsModule {
}
