import { Component, OnInit } from '@angular/core';
import { MaterialIconsService } from '../../../global/providers/material-icons.service';
import { Bank } from '../../../core/bank';
import { ActionCard } from '../../../global/consts/action-card.enum';
import { ContentService } from '../../../global/providers/content.service';

@Component({
    selector: 'app-banks',
    templateUrl: './banks.component.html',
    styleUrls: ['./banks.component.scss'],
})
export class BanksComponent implements OnInit {

    banks: Bank[] = Array.from({length: 20}, (_, index) =>
        index % 3 ?
            {bank: `Banco Universal`, name: 'Luis Perez', number: '0555 5895 88 88555 55554', type: 'Corriente'} :
            {bank: 'Banesco', name: 'Juanito Vargas', number: '5699 88 15405 76335 55554', type: 'Ahorro'},
    );

    constructor(private materialIcons: MaterialIconsService,
                private contentService: ContentService) {
    }

    ngOnInit(): void {
    }

    selectBank(item: any): void {
        this.contentService.selectCard.emit(item);
        this.contentService.actionCard.emit(ActionCard.VIEW);
    }

    searchByText(text: string): void {
        console.log(text);
    }

}
