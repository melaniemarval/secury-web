import { Component, OnInit } from '@angular/core';
import { MaterialIconsService } from '../../../global/providers/material-icons.service';
import { ActionCard } from '../../../global/consts/action-card.enum';
import { ContentService } from '../../../global/providers/content.service';
import { Card } from '../../../core/card';

@Component({
    selector: 'app-cards',
    templateUrl: './cards.component.html',
    styleUrls: ['./cards.component.scss'],
})
export class CardsComponent implements OnInit {

    cards: Card[] = Array.from({length: 20}, (_, index) =>
        index % 3 ?
            {bank: `Master Card`, name: 'Luis Perez', cardNumber: '0555 5895 88745 8855', expirationDate: '01/12'} :
            {bank: 'American Express', name: 'Juanito Vargas', cardNumber: '5699 8888 1540 7633', expirationDate: '15/06'},
    );

    constructor(private materialIcons: MaterialIconsService,
                private contentService: ContentService) {
    }

    ngOnInit(): void {
    }

    selectCard(item: any): void {
        this.contentService.selectCard.emit(item);
        this.contentService.actionCard.emit(ActionCard.VIEW);
    }

    searchByText(text: string): void {
        console.log(text);
    }

}
