import { Component, OnInit } from '@angular/core';
import { ContentService } from '../../../global/providers/content.service';
import { MaterialIconsService } from '../../../global/providers/material-icons.service';
import { ActionCard } from '../../../global/consts/action-card.enum';
import { AccountInterface } from '../../../core/accountInterface';

@Component({
    selector: 'app-accounts',
    templateUrl: './accounts.component.html',
    styleUrls: ['./accounts.component.scss'],
})
export class AccountsComponent implements OnInit {

    accounts: AccountInterface[] = Array.from({length: 20}, (_, index) => index % 3 === 0 ?
        {name: 'Instagram', email: 'correodeinstagram24@gmail.com', iconUrl: 'xac_adobe_photoshop'} :
        {name: 'Facebook', email: 'micorreo_personal@facebook.com', iconUrl: 'xac_badoo'});

    constructor(private contentService: ContentService,
                // to show custom icons
                private materialIcons: MaterialIconsService) {

    }

    ngOnInit(): void {
    }

    selectAccount(item: any): void {
        this.contentService.selectCard.emit(item);
        this.contentService.actionCard.emit(ActionCard.VIEW);
    }

    searchByText(text: string): void {
        console.log(text);
    }
}
