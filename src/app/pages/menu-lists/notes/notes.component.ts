import { Component, OnInit } from '@angular/core';
import { MaterialIconsService } from '../../../global/providers/material-icons.service';
import { Note } from '../../../core/note';
import { ActionCard } from '../../../global/consts/action-card.enum';
import { ContentService } from '../../../global/providers/content.service';

@Component({
    selector: 'app-notes',
    templateUrl: './notes.component.html',
    styleUrls: ['./notes.component.scss'],
})
export class NotesComponent implements OnInit {

    notes: Note[] = [{
        name: 'My first note',
        color: 'red',
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti iusto maxime minima sequi veritatis. A' +
            '        debitis deleniti facere numquam. Aliquid aperiam at culpa dolor laboriosam minima mollitia obcaecati odio velit.\n\n' +
            '    Corporis, sapiente velit? A, aliquam at autem consectetur cum ducimus error excepturi explicabo maiores minima' +
            '        nihil perspiciatis quisquam, quo ratione rem reprehenderit totam vero. Commodi ducimus eum id molestias placeat.' +
            '    Animi aperiam dolorem doloremque, eveniet explicabo iure labore maiores minima nam odio ut voluptates! Fugit' +
            '        illo iste nam pariatur. Atque blanditiis fugiat illo natus nobis quis quo repudiandae voluptas voluptatem!\n\n' +
            '    Adipisci consequatur cumque distinctio dolores eius est fuga, in nesciunt officiis pariatur provident, quam,' +
            '        qui quia rem tempora! Dignissimos error explicabo in magni maxime minima odio perspiciatis sed, suscipit vitae.',
    }, {
        name: 'Nota de compras',
        color: 'red',
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti iusto maxime minima sequi veritatis. A' +
            '        debitis deleniti facere numquam. Aliquid aperiam at culpa dolor laboriosam minima mollitia obcaecati odio velit',
    }, {
        name: 'Lista de preparativos',
        color: 'green',
        content: 'Lorem ipsum dolor obcaecati \n' +
            'Consectetur adipisicing elit \n' +
            'Deleniti iusto maxime minima sequi veritatis. \n' +
            'Debitis deleniti facere numquam \n' +
            'Aliquid aperiam at culpa dolor \n ' +
            'Laboriosam minima mollitia odio velit',
    }];

    constructor(private materialIcons: MaterialIconsService,
                private contentService: ContentService) {
    }

    ngOnInit(): void {
    }

    searchByText(event: string): void {
        console.log('-> event', event);
    }

    selectNote(note: Note): void {
        this.contentService.selectCard.emit(note);
        this.contentService.actionCard.emit(ActionCard.VIEW);
    }
}
