import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
    selector: 'app-searchbar',
    templateUrl: './searchbar.component.html',
    styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent implements OnInit {

    @Input() title: string;
    @Input() showSearch: boolean;
    @Output() searchByText: EventEmitter<string> = new EventEmitter<string>();

    @ViewChild('inputSearch') searchInput: ElementRef;
    isSearching: boolean;
    searchText: string;

    constructor() {
    }

    ngOnInit(): void {
    }

    showSearchbar(): void {
        console.log('-> this.searchInput', this.searchInput);
        this.isSearching = true;
        setTimeout(() => { // this will make the execution after the above boolean has changed
            this.searchInput.nativeElement.focus();
        }, 0);
    }

    searchByTextEmit(): void {
        this.searchByText.emit(this.searchText);
    }
}
