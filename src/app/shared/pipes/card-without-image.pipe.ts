import { Pipe, PipeTransform } from '@angular/core';
import { AccountInterface } from '../../core/accountInterface';

@Pipe({
    name: 'cardWithoutImage',
    pure: false
})
export class CardWithoutImagePipe implements PipeTransform {

    transform(iconUrl: string): string {
        let url = 'assets/img/blank/account-icon.jpg';
        if (iconUrl) {
            url = '/assets/icon/accounts/' + iconUrl + '.png';
        }
        return url;
    }

}
