import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardWithoutImagePipe } from './card-without-image.pipe';
import { FilterByParametersPipe } from './filter-by-parameters.pipe';


@NgModule({
    declarations: [
        CardWithoutImagePipe,
        FilterByParametersPipe
    ],
    exports: [
        CardWithoutImagePipe,
        FilterByParametersPipe,
    ],
    imports: [
        CommonModule,
    ],
})
export class PipesModule {
}
