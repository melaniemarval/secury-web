import { Component, Input, OnInit } from '@angular/core';
import { Tab } from '../../../global/consts/tab.enum';

@Component({
    selector: 'app-without-card',
    templateUrl: './without-card.component.html',
    styleUrls: ['./without-card.component.scss'],
})
export class WithoutCardComponent implements OnInit {

    @Input() tab: number;
    imgUrl: string;

    constructor() {
    }

    ngOnInit(): void {
        switch (this.tab) {
            case Tab.ACCOUNTS:
                this.imgUrl = 'assets/img/blank/clipboard.svg';
                break;
            case Tab.CARDS:
                this.imgUrl = 'assets/img/blank/clipboard.svg';
                break;
            case Tab.BANKS:
                this.imgUrl = 'assets/img/blank/clipboard.svg';
                break;
            default:
                this.imgUrl = 'assets/img/blank/clipboard.svg';
        }
    }

}
