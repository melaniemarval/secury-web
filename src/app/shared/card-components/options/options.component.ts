import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActionCard } from '../../../global/consts/action-card.enum';
import { ThemePalette } from '@angular/material/core';

@Component({
    selector: 'app-options',
    templateUrl: './options.component.html',
    styleUrls: ['./options.component.scss'],
})
export class OptionsComponent implements OnInit {

    @Input() action: ActionCard;

    @Input() showColorButton?: boolean;
    @Input() color?: string;
    @Output() editColor: EventEmitter<string> = new EventEmitter<string>();

    @Input() showCopyButton?: boolean;
    @Output() copyText: EventEmitter<any> = new EventEmitter<any>();

    @Output() editCard: EventEmitter<any> = new EventEmitter<any>();
    @Output() deleteCard: EventEmitter<any> = new EventEmitter<any>();
    actionCardType = ActionCard;

    constructor() {
    }

    ngOnInit(): void {
    }

    copyToClipboard(): void {
        this.copyText.emit();
    }

    goToChangeColor(): void {
        this.editColor.emit(this.color);
    }

    goToEdit(): void {
        this.editCard.emit();
    }

    confirmDelete(): void {
        this.deleteCard.emit();
    }

}
