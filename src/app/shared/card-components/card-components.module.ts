import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WithoutCardComponent } from './without-card/without-card.component';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { AppMaterialModule } from '../../global/app.material.module';
import { PipesModule } from '../pipes/pipes.module';
import { FormsModule } from '@angular/forms';
import { OptionsComponent } from './options/options.component';
import { ColorPickerModule } from 'ngx-color-picker';



@NgModule({
  declarations: [
      ConfirmDeleteComponent,
      WithoutCardComponent,
      OptionsComponent,
  ],
    imports: [
        CommonModule,
        AppMaterialModule,
        PipesModule,
        FormsModule,
        ColorPickerModule,
    ],
  exports: [
      ConfirmDeleteComponent,
      WithoutCardComponent,
      OptionsComponent,
  ]
})
export class CardComponentsModule { }
