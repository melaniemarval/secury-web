import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu.component';
import { AppMaterialModule } from '../../global/app.material.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ToolbarModule } from '../toolbar/toolbar.module';
import { NgMatSearchBarModule } from 'ng-mat-search-bar';
import { FormsModule } from '@angular/forms';
import { ContentComponentModule } from '../../pages/content/content-component.module';
import { MenuListsModule } from '../../pages/menu-lists/menu-lists.module';



@NgModule({
    declarations: [MenuComponent],
    exports: [
        MenuComponent,
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        AppMaterialModule,
        RouterModule,
        ToolbarModule,
        NgMatSearchBarModule,
        FormsModule,
        ContentComponentModule,
        MenuListsModule,
    ],
})
export class MenuModule { }
