import { ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MaterialIconsService } from '../../global/providers/material-icons.service';
import { ContentService } from '../../global/providers/content.service';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class MenuComponent implements OnInit, OnDestroy {

    @ViewChild('snav') sidenav: MatSidenav;
    private tabSelected: number;
    mobileQuery: any;
    mobileQueryListener: () => void;
    widthScreen: number;

    constructor(private changeDetectorRef: ChangeDetectorRef,
                private media: MediaMatcher,
                private materialIcons: MaterialIconsService, // to show custom icons
                private contentService: ContentService) {

        this.widthScreen = window.innerWidth;
        // this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        // console.log('-> this.mobileQuery', this.mobileQuery);
        // this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
        // this.mobileQuery.addListener(this.mobileQueryListener);
    }

    ngOnInit(): void {
        this.contentService.selectCard.subscribe(card => {
           if (card && this.widthScreen < 724) {
               this.sidenav.close();
           }
        });
    }

    @HostListener('window:resize', ['$event'])
    onResize(event): void {
        this.widthScreen = window.innerWidth;
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this.mobileQueryListener);
    }

    selectTab(event: number): void {
        this.tabSelected = event;
        this.contentService.changeTab.emit(event);
        this.contentService.selectCard.emit(undefined);
    }

    searchByText(text: string): void {
        console.log(text);
    }


}
