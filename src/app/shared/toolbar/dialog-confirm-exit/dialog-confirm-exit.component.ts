import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-dialog-confirm-exit',
    templateUrl: './dialog-confirm-exit.component.html',
    styleUrls: ['./dialog-confirm-exit.component.scss'],
})
export class DialogConfirmExitComponent implements OnInit {

    constructor(private dialogRef: MatDialogRef<DialogConfirmExitComponent>) {
    }

    ngOnInit(): void {
    }

    closeDialog(): void {
        this.dialogRef.close();
    }

    closeDialogWithParam(): void {
        this.dialogRef.close('yes');
    }
}
