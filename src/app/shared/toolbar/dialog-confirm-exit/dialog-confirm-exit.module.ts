import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogConfirmExitComponent } from './dialog-confirm-exit.component';
import { AppMaterialModule } from '../../../global/app.material.module';



@NgModule({
  declarations: [DialogConfirmExitComponent],
    imports: [
        CommonModule,
        AppMaterialModule,
    ],
})
export class DialogConfirmExitModule { }
