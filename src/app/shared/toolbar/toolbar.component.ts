import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ConfirmDeleteComponent } from '../card-components/confirm-delete/confirm-delete.component';
import { MatDialog } from '@angular/material/dialog';
import { DialogConfirmExitComponent } from './dialog-confirm-exit/dialog-confirm-exit.component';

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {

    @Output() openMenu: EventEmitter<any> = new EventEmitter<any>();

    constructor(private dialog: MatDialog) {
    }

    ngOnInit(): void {
    }

    eventOpenMenu(): void {
        this.openMenu.emit();
    }

    confirmCloseSession(): void {
        const dialogRef = this.dialog.open(DialogConfirmExitComponent);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.closeSession();
            }
        });
    }

    closeSession(): void {
        console.log('cerrar sesion');
    }

}
