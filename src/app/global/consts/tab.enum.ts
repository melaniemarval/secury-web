export enum Tab {
    ACCOUNTS = 0,
    BANKS = 1,
    CARDS = 2,
    NOTES = 3
}
