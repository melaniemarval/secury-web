import { EventEmitter, Injectable } from '@angular/core';
import { ActionCard } from '../consts/action-card.enum';
import { Tab } from '../consts/tab.enum';

@Injectable({
    providedIn: 'root',
})
export class ContentService {

    public changeTab: EventEmitter<Tab> = new EventEmitter<Tab>();

    public selectCard: EventEmitter<any> = new EventEmitter<any>();

    public actionCard: EventEmitter<ActionCard> = new EventEmitter<ActionCard>();

    constructor() {
    }

}
