import { Injectable } from '@angular/core';


const ACCESS_TOKEN = 'access-token';

@Injectable({
    providedIn: 'root',
})
export class StorageService {

    constructor() {
    }

    setAccessToken(accessToken: string): void {
        localStorage.setItem(ACCESS_TOKEN, accessToken);
    }

    getAccessToken(): string {
        return localStorage.getItem(ACCESS_TOKEN);
    }
}
