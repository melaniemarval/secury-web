import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
    providedIn: 'root',
})
export class MaterialIconsService {

    constructor(private iconRegistry: MatIconRegistry,
                private sanitizer: DomSanitizer) {

        // iconRegistry.addSvgIcon('credit-card',
        //     sanitizer.bypassSecurityTrustResourceUrl('assets/icon/accounts/credit-card.svg'));

        iconRegistry.addSvgIcon('google-icon',
            sanitizer.bypassSecurityTrustResourceUrl('assets/icon/google-icon.svg'));
    }
}
