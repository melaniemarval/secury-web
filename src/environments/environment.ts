// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,

    firebase: {
        apiKey: 'AIzaSyDox1YNpMBJgii_6ZUavK1Bh9G7rOh24IM',
        authDomain: 'secury-app.firebaseapp.com',
        databaseURL: 'https://secury-app.firebaseio.com',
        projectId: 'secury-app',
        storageBucket: 'secury-app.appspot.com',
        messagingSenderId: '147829971635',
        appId: '1:147829971635:web:ffdd992826500afba9aec6',
        measurementId: 'G-PNM5PVX6TH',
    },
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
